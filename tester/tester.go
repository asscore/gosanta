package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"gosanta/base"
	"gosanta/utils"
)

var location base.Location

func main() {
	var lat, lng float64
	var dev bool
	// domyślna lokalizacja Poznań (Polska)
	flag.Float64Var(&lat, "lat", 52.408702, "Latitude")
	flag.Float64Var(&lng, "lng", 16.924438, "Longitude")
	flag.BoolVar(&dev, "d", false, "Dev mode")
	flag.Parse()

	location = base.Location{Lat: lat, Lng: lng}

	request, err := http.NewRequest("GET", "http://localhost:8000/santa", nil)
	if err != nil {
		log.Fatal(err)
	}

	query := request.URL.Query()

	timeStamp := utils.CurrentTimeStamp()
	if dev {
		timeStamp = utils.FakeCurrentTimeStamp()
	}

	query.Add("timestamp", fmt.Sprintf("%v", timeStamp))
	query.Add("lat", fmt.Sprintf("%f", location.Lat))
	query.Add("lng", fmt.Sprintf("%f", location.Lng))
	request.URL.RawQuery = query.Encode()

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	var destination base.Destination
	if err := json.Unmarshal(body, &destination); err != nil {
		log.Fatalf("unmarshaling error: %v", err)
	}

	arrival := destination.Arrival.Date().Format("02.01.2006 15:04:05")
	departure := destination.Departure.Date().Format("02.01.2006 15:04:05")
	log.Printf("Arrival: %s Departure: %s City: %s (%s) Distance: %0.2fkm", arrival, departure, destination.City,
		destination.Region, destination.Distance)
}
