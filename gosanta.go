package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"sync"

	"gosanta/base"
)

type Json struct {
	Destinations []base.Destination
}

func handler(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Server", "Santa Tracker")
	w.WriteHeader(200)
}

var mu sync.Mutex
var js Json

func CheckQuery(name string, values url.Values) bool {
	keys, ok := values[name]
	if !ok || len(keys[0]) < 1 {
		return false
	}

	return true
}

func handlerSanta(w http.ResponseWriter, r *http.Request) {
	mu.Lock()

	var data []byte

	q := r.URL.Query()
	if CheckQuery("timestamp", q) {
		ts, err := strconv.ParseInt(q["timestamp"][0], 10, 64)
		if err == nil {
			timeStamp := base.TimeStamp(ts)

			var dst base.Destination
			for _, destination := range js.Destinations {
				if timeStamp < destination.Arrival {
					break
				}
				dst = destination
			}

			var lat float64
			var lng float64
			if CheckQuery("lat", q) && CheckQuery("lng", q) {
				lat, err = strconv.ParseFloat(q["lat"][0], 64)
				if err == nil {
					lng, err = strconv.ParseFloat(q["lng"][0], 64)
					if err == nil {
						location := base.Location{Lat: lat, Lng: lng}
						dst.Distance = location.Distance(dst.Location)
					}
				}
			}

			date := timeStamp.Date().Format("02.01.2006 15:04:05")
			log.Printf("[/santa] Lat: %f Lng: %f, Date: %s City: %s (%s) Distance: %0.2fkm", lat, lng, date,
				dst.City, dst.Region, dst.Distance)
			data, _ = json.Marshal(dst)
		}
	} else {
		data = []byte("{}")
	}

	w.Header().Set("Content-Type", "application/json")
	//noinspection GoUnhandledErrorResult
	w.Write(data)

	mu.Unlock()
}

func main() {
	flog, err := os.Create("gosanta.log")
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	//noinspection GoUnhandledErrorResult
	defer flog.Close()

	log.SetOutput(io.MultiWriter(os.Stdout, flog))

	santaJson, err := ioutil.ReadFile("santa_pl.json")
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(santaJson, &js); err != nil {
		log.Fatalf("unmarshaling error: %v", err)
	}

	for i := 0; i < len(js.Destinations); i++ {
		if js.Destinations[i].Id == "landing" {
			js.Destinations[0].Arrival = js.Destinations[i].Arrival
			js.Destinations[0].Arrival.ToPrevYear()
			js.Destinations[i].Departure.ToNextYear()
		} else {
			js.Destinations[i].Arrival.ToCurrentYear()
			js.Destinations[i].Departure.ToCurrentYear()
		}
	}

	http.HandleFunc("/", handler)
	http.HandleFunc("/santa", handlerSanta)
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}
