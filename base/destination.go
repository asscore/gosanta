package base

type Destination struct {
	Id                string
	Arrival           TimeStamp
	Departure         TimeStamp
	Population        int64
	PresentsDelivered int64
	City              string
	Region            string
	Location          Location

	Distance float64
}
