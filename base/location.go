package base

import "math"

type Location struct {
	Lat float64
	Lng float64
}

// http://en.wikipedia.org/wiki/Haversine_formula
func (origin Location) Distance(destination Location) float64 {
	var la1, lo1, la2, lo2, r float64
	la1 = origin.Lat * math.Pi / 180
	lo1 = origin.Lng * math.Pi / 180
	la2 = destination.Lat * math.Pi / 180
	lo2 = destination.Lng * math.Pi / 180

	r = 6378100 // promień Ziemi w metrach

	// haversin(θ)
	hsin := func(theta float64) float64 {
		return math.Pow(math.Sin(theta/2), 2)
	}

	h := hsin(la2-la1) + math.Cos(la1)*math.Cos(la2)*hsin(lo2-lo1)

	return (2 * r * math.Asin(math.Sqrt(h))) / 1000
}
