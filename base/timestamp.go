package base

import "time"

type TimeStamp int64

func (timeStamp *TimeStamp) ToCurrentYear() {
	d := time.Unix(int64(*timeStamp)/1000, 0)
	d = d.AddDate(time.Now().Year()-d.Year(), 0, 0)

	*timeStamp = TimeStamp(d.Unix() * 1000)
}

func (timeStamp *TimeStamp) ToNextYear() {
	d := time.Unix(int64(*timeStamp)/1000, 0)
	d = d.AddDate(time.Now().Year()-d.Year()+1, 0, 0)

	*timeStamp = TimeStamp(d.Unix() * 1000)
}

func (timeStamp *TimeStamp) ToPrevYear() {
	d := time.Unix(int64(*timeStamp)/1000, 0)
	d = d.AddDate(time.Now().Year()-d.Year()-1, 0, 0)

	*timeStamp = TimeStamp(d.Unix() * 1000)
}

func (timeStamp TimeStamp) Date() time.Time {
	return time.Unix(int64(timeStamp)/1000, 0)
}
