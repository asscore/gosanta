package utils

import (
	"time"

	"gosanta/base"
)

func CurrentTimeStamp() base.TimeStamp {
	return base.TimeStamp(time.Now().Unix() * 1000)
}

func FakeCurrentTimeStamp() base.TimeStamp {
	d := time.Now()
	d = d.AddDate(0, 12-int(d.Month()), 24-d.Day())

	return base.TimeStamp(d.Unix() * 1000)
}
